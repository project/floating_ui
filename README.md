# Floating UI

This module provides the Floating UI library for
any themes and modules that require it.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/floating_ui).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/floating_ui).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

This module does not have any configuration.


## Maintainers

- JurriaanRoelofs - [JurriaanRoelofs](https://www.drupal.org/u/jurriaanroelofs)
- Neslee Canil Pinto - [Neslee Canil Pinto](https://www.drupal.org/u/neslee-canil-pinto)
